<%@ page import="pl.sda.store.Main.model.Product" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Szymon
  Date: 23.07.2019
  Time: 20:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product List</title>
</head>
<body>
<jsp:include page="/header.jsp"></jsp:include>

<table>
    <tr>
        <th style="width: 150px;">Name</th>
        <th style="width: 150px;">ID</th>
    </tr>
    <%
        List<Product> productList = (List<Product>) request.getAttribute("productList");

        for (Product product : productList) {
            out.print("<tr>");
            out.print("<td>" + product.getName() + "</td>");
            out.print("<td>" + product.getId() + "</td>");
            out.print("<td>" + "<a href=\"/inventory/add?id=" + product.getId() + "\">" + "Dodaj stan magazynowy</a>" + "</td>");
            out.print("<td>" + "<a href=\"/inventory/list?id=" + product.getId() + "\">" + "Wyświetl stan magazynowy</a>" + "</td>");

            out.print("</tr>");
        }

    %>
</table>
</body>
</html>

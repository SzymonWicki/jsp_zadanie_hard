<%--
  Created by IntelliJ IDEA.
  User: Szymon
  Date: 23.07.2019
  Time: 20:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Inventory Add</title>
</head>
<body>
<jsp:include page="/header.jsp"></jsp:include>

<form action="/inventory/add" method="post">
    <input type="hidden" name="id" value="<%=request.getAttribute("id")%>" readonly>
    <input type="number" name="inventoryQuantity" placeholder="Inventory Quantity">
    <input type="number" name="inventoryValue" placeholder="Inventory Value">
    <br>
    <br>
    <input type="submit" value="Submit">

</form>
</body>
</html>

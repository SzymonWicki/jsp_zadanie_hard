<%@ page import="pl.sda.store.Main.model.Product" %>
<%@ page import="java.util.List" %>
<%@ page import="pl.sda.store.Main.model.Inventory" %><%--
  Created by IntelliJ IDEA.
  User: Szymon
  Date: 23.07.2019
  Time: 20:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Inventory List</title>
</head>
<body>
<jsp:include page="/header.jsp"></jsp:include>

<table>
    <tr>
        <th style="width: 150px;">ID</th>
        <th style="width: 150px;">Date Arrived</th>
        <th style="width: 150px;">Quantity</th>
        <th style="width: 150px;">Value</th>
    </tr>
    <%
        List<Inventory> inventoryList = (List<Inventory>) request.getAttribute("inventoryList");

        for (Inventory inventory : inventoryList) {
            out.print("<tr>");
            out.print("<td>" + inventory.getId() + "</td>");
            out.print("<td>" + inventory.getDateArrived() + "</td>");
            out.print("<td>" + inventory.getQuantity() + "</td>");
            out.print("<td>" + inventory.getValue() + "</td>");

            out.print("</tr>");
        }

    %>
</table>
</body>
</html>

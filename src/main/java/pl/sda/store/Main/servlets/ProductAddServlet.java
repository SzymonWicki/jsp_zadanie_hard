package pl.sda.store.Main.servlets;

import pl.sda.store.Main.database.EntityDao;
import pl.sda.store.Main.model.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/product/add")
public class ProductAddServlet extends HttpServlet {

    private EntityDao entityDao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/formularze/productAdd.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        Product product = new Product();

        product.setName(req.getParameter("productName"));

        entityDao.saveOrUpdate(product);

        resp.sendRedirect("/product/add");
    }
}

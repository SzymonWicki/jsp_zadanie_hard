package pl.sda.store.Main.servlets;

import pl.sda.store.Main.database.EntityDao;
import pl.sda.store.Main.model.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/product/list")
public class ProductListServlet extends HttpServlet {

    private EntityDao entityDao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("productList", entityDao.findAll(Product.class));
        req.getRequestDispatcher("/formularze/productList.jsp").forward(req, resp);
    }
}

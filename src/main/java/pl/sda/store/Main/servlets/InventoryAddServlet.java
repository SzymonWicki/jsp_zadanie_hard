package pl.sda.store.Main.servlets;

import pl.sda.store.Main.database.EntityDao;
import pl.sda.store.Main.model.Inventory;
import pl.sda.store.Main.model.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@WebServlet("/inventory/add")
public class InventoryAddServlet extends HttpServlet {

    private EntityDao entityDao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String productId = req.getParameter("id");

        req.setAttribute("id", productId);
        req.getRequestDispatcher("/formularze/inventoryAdd.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        Inventory inventory = new Inventory();

        final String id = req.getParameter("id");

        inventory.setQuantity(Double.parseDouble(req.getParameter("inventoryQuantity")));
        inventory.setValue(Double.parseDouble(req.getParameter("inventoryValue")));

        final Optional<Product> optionalProduct = entityDao.findById(Product.class, Long.parseLong(id));
        System.out.println(optionalProduct.get().getId());

        if (optionalProduct.isPresent()) {
            Product product = optionalProduct.get();
            inventory.setProduct(product);

            entityDao.saveOrUpdate(inventory);
            product.getInventories().add(inventory);
            entityDao.saveOrUpdate(product);
        }

        entityDao.saveOrUpdate(inventory);


        resp.sendRedirect("/product/list");
    }
}

package pl.sda.store.Main.servlets;

import pl.sda.store.Main.database.EntityDao;
import pl.sda.store.Main.model.Inventory;
import pl.sda.store.Main.model.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/inventory/list")
public class InventoryListServlet extends HttpServlet {

    private EntityDao entityDao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");

        if (id != null){
            final Optional<Product> optionalProduct = entityDao.findById(Product.class, Long.parseLong(id));
            if (optionalProduct.isPresent()) {
                req.setAttribute("inventoryList", optionalProduct.get().getInventories());
            } else {
                System.err.println("Nie udało się znaleźć Produktu");
            }
        } else {
            req.setAttribute("inventoryList", entityDao.findAll(Inventory.class));
        }
        req.getRequestDispatcher("/formularze/inventoryList.jsp").forward(req, resp);

    }
}
